var app = new Vue({
  el: '#app',
  data: {
    login: '',
    password: '',
    error: false,
    success: false,
  },
  methods: {
  	loginHandle(e) {
  		if (this.login.length > 0 && this.password.length > 0) {
  		    var that = this;
			var payload = {
                "jsonrpc": "2.0",
                "method": "auth.login",
                "params": {
                    "login": this.login,
                    "password": this.password,
                }
            };

			var url = '/api/jsonrpc';
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(payload)
            })
                .then(response => response.json())
                .then(function (jsonrpc) {
                    if (jsonrpc.error) {
                        that.showError(jsonrpc.error.message);
                    } else if(jsonrpc.result) {
                        if (jsonrpc.result.status) {
                            that.showSuccess('Успешная авторизация');
                        } else {
                            that.showError('Неверный логин или пароль');
                        }
                    } else {
                        that.showError('Сервис временно не работает');
                    }
                });
  		} else {
  			this.showError("Введите логин и пароль");
  		}
  	},
  	showError(text) {
  		this.success = false;
  		this.error = text;
  	},
  	showSuccess(text) {
  		this.success = text;
  		this.error = false;
  	}
  }
})