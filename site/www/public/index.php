<?php

use Phalcon\Mvc\Micro;

$app = new Micro();

$app['view'] = function () {
    $view = new \Phalcon\Mvc\View\Simple();
    $view->setViewsDir('../app/views/');
    return $view;
};

$app->get(
    '/',
    function () use ($app) {
        echo $app['view']->render(
            'index'
        );
    }
);

$app->handle();