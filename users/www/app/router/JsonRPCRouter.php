<?php

use Phalcon\Mvc\Router;

class JsonRPCRouter extends Router
{
    protected $_data;

    public function __construct($data = null)
    {
        $this->_data = $data;
    }

    /**
     * Парсинг jsonRPC запроса
     * @param null $data
     */
    public function handle($data = null)
    {
        if (!$data) {
            $data = $this->_data;
        }

        $data = json_decode($data, true);

        if (!isset($data['jsonrpc'])) {
            throw new Phalcon\Mvc\Router\Exception("The request is not Json-RPC");
        }

        $method = explode('.', $data['method']);

        $this->_controller = $method[0];
        $this->_action = $method[1];
        $this->_params = $data['params'];
    }

}