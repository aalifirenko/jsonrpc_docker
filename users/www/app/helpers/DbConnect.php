<?php

use Phalcon\Db\Adapter\Pdo\Sqlite as Sqlite;

class DbConnect {

    private static $_instance = null;
    private $db = null;

    /**
     * @return UserCore|null
     */
    static public function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function sqlite()
    {
        if (!$this->db) {
            $this->db = new Sqlite(['dbname' => BASE_PATH . "/db/users.sqlite"]);
        }

        return $this->db;
    }
}