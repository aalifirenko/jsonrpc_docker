<?php

use Phalcon\Mvc\Controller;

class AuthController extends Phalcon\Mvc\Controller {

    public function loginAction($login, $password)
    {
        $status = true;
        $message = 'success';

        $users = new Users();

        $check_user = $users->findFirst([
            "login = :login: AND password = :password:",
            'bind' => [
                'login' => $login,
                'password' => $password,
            ]
        ]);


        if ($check_user === false) {
            $status = false;
            $message = 'wrong login or password';
        }

        $response = new JsonRPCResponse();
        $response->result = [
            'status' => $status,
            'message' => $message,
        ];

        echo $response;
    }

}