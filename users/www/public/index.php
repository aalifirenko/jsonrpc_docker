<?php

use Phalcon\Mvc\Micro as Micro;
use Phalcon\Loader as Loader;
use Phalcon\Di as Di;
use Phalcon\Db\Adapter\Pdo\Sqlite as Sqlite;

define('BASE_PATH', dirname(__DIR__));

try {

    $paths = [
        '../app/controllers/',
        '../app/router/',
        '../app/response/',
        '../app/models/',
    ];

    // Загружаем необходимые классы
    $loader = new Loader();
    $loader->registerDirs($paths)
        ->register();


    // Инициализируем DI
    $di = new Di();
    $di->set('dispatcher', 'Phalcon\Mvc\Dispatcher');
    $di->set('response', 'Phalcon\Http\Response');
    $di->set('modelsManager', 'Phalcon\Mvc\Model\Manager');
    $di->set('modelsMetadata', 'Phalcon\Mvc\Model\Metadata\Memory');
    $di->set('db', function () {
        return new Sqlite(['dbname' => BASE_PATH . "/db/users.sqlite"]);
    });

    $app = new Micro();

    /*
     * Идея такая, на Api Url приходит JsonRPC запрос
     * JsonRPCRouter роутер парсит его, получает Controller, Action, Params
     * Далее через Di + Dispatcher запускаем вручную метод контроллера
     */
    $app->post(
        '/',
        function () use ($app, $di) {
            $rpcRouter = new JsonRPCRouter();
            $rpcRouter->handle($app->request->getRawBody());

            $dispatcher = $di->getShared('dispatcher');
            $dispatcher->setControllerName($rpcRouter->getControllerName());
            $dispatcher->setActionName($rpcRouter->getActionName());
            $dispatcher->setParams($rpcRouter->getParams());
            $dispatcher->dispatch();
        }
    );

    $app->notFound(
        function () use ($app) {
            throw new Exception('Api Endpoint not found');
        }
    );

    $app->handle();

} catch (\Exception $e) {
    $response = new JsonRPCResponse();
    $response->error = $e;
    echo $response;
}